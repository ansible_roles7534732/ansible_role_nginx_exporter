Nginx_exporter role
=========

Installs Nginx-prometheus-exporter(https://github.com/nginxinc/nginx-prometheus-exporter)

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install nginx_exporter
  hosts: web_vms
  become: true
  gather_facts: true
  roles:
    - nginx_exporter
      tags:
        - role_nginx_exporter
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
